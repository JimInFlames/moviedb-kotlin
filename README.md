Fetching the upcoming movies via MovieDB, with endless scroll and search.

Technologies used :
Kotlin, MVVM, Retrofit, Lifecycle, LiveData, androidx Paging, Glide, and for dependancy injection koin.