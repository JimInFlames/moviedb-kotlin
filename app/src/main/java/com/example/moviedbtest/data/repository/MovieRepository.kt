package com.example.moviedbtest.data.repository

import com.example.moviedbtest.network.api.MovieService
import com.example.moviedbtest.network.response.MovieListResponse
import com.example.moviedbtest.network.api.Result


class MovieRepository(private val movieService: MovieService) {

    suspend fun getLatestMovies(page: Int): MovieListResponse {
        return when (val result = movieService.getLatestMovies(page)) {
            is Result.Error -> MovieListResponse()
            is Result.Success -> result.data
        }
    }

    suspend fun searchMovies(searchQuery: String): MovieListResponse {
        return when (val result = movieService.searchForMovies(searchQuery)) {
            is Result.Error -> MovieListResponse()
            is Result.Success -> result.data
            else -> MovieListResponse()
        }
    }

}