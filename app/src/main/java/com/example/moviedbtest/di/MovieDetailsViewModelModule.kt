package com.example.moviedbtest.di

import com.example.moviedbtest.view.dedicated.MovieDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 *  Our View model module, for the dedicated screen
 */
val detailViewModelModule = module {
    viewModel { MovieDetailsViewModel(get()) }
}
