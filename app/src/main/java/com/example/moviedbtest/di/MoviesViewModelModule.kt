package com.example.moviedbtest.di

import com.example.moviedbtest.view.movies.LatestMoviesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 *  Our View model module, for the movies screen
 */
val viewModelModule = module {
    viewModel { LatestMoviesViewModel(get()) }
}
