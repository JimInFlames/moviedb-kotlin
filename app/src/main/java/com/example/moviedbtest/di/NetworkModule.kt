package com.example.moviedbtest.di

import com.example.moviedbtest.network.Endpoints
import com.example.moviedbtest.network.api.ApiService
import com.example.moviedbtest.network.api.MovieService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton instances of our network module
 */
val networkModule = module {
    single { retrofit() }
    single { apiService(get()) }
    single { createMovieAppService(get()) }
}

fun createMovieAppService(
    apiService: ApiService
): MovieService = MovieService(apiService)

fun apiService(retrofit: Retrofit): ApiService =
    retrofit.create(ApiService::class.java)

fun retrofit(): Retrofit = Retrofit.Builder()
    .baseUrl(Endpoints.BASE_URL)
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(GsonConverterFactory.create())
    .build()
