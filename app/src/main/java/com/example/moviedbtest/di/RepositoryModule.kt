package com.example.moviedbtest.di

import com.example.moviedbtest.data.repository.MovieRepository
import com.example.moviedbtest.network.api.MovieService
import org.koin.dsl.module

/**
 * Singleton repository module
 */
val repositoryModule = module {
    single { createRepository(get()) }
}

fun createRepository(
    movieService: MovieService
): MovieRepository = MovieRepository(movieService)