package com.example.moviedbtest.di

import com.example.moviedbtest.view.search.SearchMoviesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Our View model module, for the search screen
 */
val searchViewModelModule = module {
    viewModel { SearchMoviesViewModel(get()) }
}