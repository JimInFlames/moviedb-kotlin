package com.example.moviedbtest.network

object Endpoints {
    const val API_KEY = "96e43a52db460fe4edb548b273d50892"
    const val BASE_URL = "https://api.themoviedb.org/"
    const val SEARCH_URL = "/3/search/movie"

    /*
    This challenge requires the latest movies, but according to https://developers.themoviedb.org/3/movies/get-latest-movie the API associated with the
    latest, returns a single movie and not a list. So for now switched to upcoming movies URL
     */
    const val UPCOMING_URL = "3/movie/upcoming"
}