package com.example.moviedbtest.network.api

import com.example.moviedbtest.network.Endpoints
import com.example.moviedbtest.network.response.MovieListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(Endpoints.UPCOMING_URL)
    suspend fun getLatestMovies(
        @Query("api_key") api_key: String,
        @Query("page") page: Int
    ): Response<MovieListResponse>

    @GET(Endpoints.SEARCH_URL)
    suspend fun searchForMovies(
        @Query("api_key") api_key: String,
        @Query("query") searchQuery: String
    ): Response<MovieListResponse>
}