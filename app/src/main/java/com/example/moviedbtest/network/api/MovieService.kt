package com.example.moviedbtest.network.api

import com.example.moviedbtest.network.Endpoints
import com.example.moviedbtest.network.response.MovieListResponse


class MovieService(private val apiService: ApiService) : BaseService() {

    suspend fun getLatestMovies(page: Int): Result<MovieListResponse> {
        return createCall { apiService.getLatestMovies(Endpoints.API_KEY, page) }
    }

    suspend fun searchForMovies(searchQuery: String): Result<MovieListResponse> {
        return createCall { apiService.searchForMovies(Endpoints.API_KEY, searchQuery) }
    }

}