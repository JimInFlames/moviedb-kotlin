package com.example.moviedbtest.network.response
import com.example.moviedbtest.data.model.Movie

class MovieListResponse : ListResponse<Movie>()
