package com.example.moviedbtest.util

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.data.repository.MovieRepository
import com.example.moviedbtest.network.response.MovieListResponse

/**
 * We are going to use PagingSource to achieve endless scrolls of the movies, using our repository
 */
class MoviesPagingSource(private val repository: MovieRepository) :
    PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        return try {
            val nextPage = params.key ?: 1
            val movieListResponse: MovieListResponse = repository.getLatestMovies(nextPage)

            LoadResult.Page(
                data = movieListResponse.results!!,
                prevKey = if (nextPage == 1) null else nextPage.minus(1),
                nextKey = if (nextPage < movieListResponse.totalPages!!)
                    movieListResponse.page?.plus(1) else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        TODO("Not yet implemented")
    }

}