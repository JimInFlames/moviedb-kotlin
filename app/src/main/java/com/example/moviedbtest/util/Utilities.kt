package com.example.moviedbtest.util

class Utilities {
    companion object {
        fun getImageUrl(imagePath: String) = "https://image.tmdb.org/t/p/w200$imagePath"
        fun getLargeImageUrl(imagePath: String) = "https://image.tmdb.org/t/p/w500$imagePath"
    }
}