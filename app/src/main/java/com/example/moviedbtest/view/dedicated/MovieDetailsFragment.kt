package com.example.moviedbtest.view.dedicated

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.databinding.FragmentMovieDetailsBinding
import com.example.moviedbtest.util.Utilities
import org.koin.androidx.viewmodel.ext.android.viewModel


class MovieDetailsFragment : Fragment() {

    private lateinit var movie: Movie // Current movie being showed in this fragment
    private lateinit var binding: FragmentMovieDetailsBinding
    private val viewModel: MovieDetailsViewModel by viewModel() // We could get more data from the API for a single movie


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // get the movie from the bundle/args
        movie = MovieDetailsFragmentArgs.fromBundle(requireArguments()).movie

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Populate movie UI with the data we got from the list
         */
        movie.run {
            binding.movieLongDescription.text = this.overview
            binding.movieTitle.text = this.title
            binding.releaseDate.text = this.releaseDate
            binding.ratingBar.rating = this.voteAverage!!.div(2)
            binding.movieRating.text = this.voteAverage.toString()

            context?.let {
                Glide
                    .with(it)
                    .load(this.backdropPath?.let { it1 ->
                        Utilities.getLargeImageUrl(it1)
                    })
                    .centerCrop()
                    .into(binding.imageViewBackGround)

                Glide
                    .with(it)
                    .load(movie.posterPath?.let { it1 ->
                        Utilities.getImageUrl(it1)
                    })
                    .into(binding.imageViewSmallImage)
            }

        }

    }

}
