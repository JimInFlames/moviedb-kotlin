package com.example.moviedbtest.view.dedicated

import androidx.lifecycle.ViewModel
import com.example.moviedbtest.data.repository.MovieRepository

class MovieDetailsViewModel(private val movieRepository: MovieRepository) : ViewModel()