package com.example.moviedbtest.view.movies

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedbtest.R
import com.example.moviedbtest.databinding.FragmentLatestMoviesBinding
import kotlinx.android.synthetic.main.fragment_latest_movies.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class LatestMoviesFragment : Fragment() {

    private lateinit var binding: FragmentLatestMoviesBinding
    private lateinit var movieAdapter: MovieAdapter
    private val viewModel: LatestMoviesViewModel by sharedViewModel()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieAdapter = MovieAdapter(MovieAdapter.MovieComparator)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLatestMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_activity_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    /**\
     * Handle menu clicks
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // navigate to search fragment
            R.id.app_bar_search -> view?.let {
                Navigation.findNavController(it)
                    .navigate(LatestMoviesFragmentDirections.actionLatestMoviesFragmentToSearchFragment())
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewMovies.adapter = movieAdapter

        // Init recycler with adapter, using LoadState
        binding.recyclerViewMovies.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = movieAdapter.withLoadStateFooter(
                footer = MovieLoadStateAdapter { movieAdapter.retry() }
            )
        }

        // Get the latest movies and append them to our adapter async
        lifecycleScope.launch {
            viewModel.getLatestMoviesFlow().collectLatest {
                movieAdapter.submitData(it)
            }
        }

        binding.buttonRetry.setOnClickListener {
            lifecycleScope.launch {
                viewModel.getLatestMoviesFlow().collectLatest { movieAdapter.submitData(it) }
            }
        }

        // Loading state for the first load
        movieAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading) {
                binding.progressBar.visibility = View.VISIBLE
                binding.recyclerViewMovies.visibility = View.GONE
                binding.buttonRetry.visibility = View.GONE

            } else {
                binding.progressBar.visibility = View.GONE
                binding.recyclerViewMovies.visibility = View.VISIBLE
                binding.buttonRetry.visibility = View.GONE
            }
            if (loadState.refresh is LoadState.Error) {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
                binding.progressBar.visibility = View.GONE
                binding.recyclerViewMovies.visibility = View.GONE
                binding.buttonRetry.visibility = View.VISIBLE
            }
        }
    }
}