package com.example.moviedbtest.view.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.data.repository.MovieRepository
import com.example.moviedbtest.util.MoviesPagingSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LatestMoviesViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    fun getLatestMoviesFlow(): Flow<PagingData<Movie>> {
        return getMovieListStream(MoviesPagingSource(repository = movieRepository))
            .map { pagingData -> pagingData.map { it } }
    }

    private fun getMovieListStream(moviePagingSource: MoviesPagingSource): Flow<PagingData<Movie>> {
        return Pager(PagingConfig(20)) { // Going to use 20 as page size
            moviePagingSource
        }.flow
            .cachedIn(viewModelScope)
    }
}