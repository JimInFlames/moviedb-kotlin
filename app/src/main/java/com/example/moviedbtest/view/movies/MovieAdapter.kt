package com.example.moviedbtest.view.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.databinding.ListItemLayoutBinding
import com.example.moviedbtest.util.Utilities

/**
 * Movie Adapter class, passing movie data to the view components
 */
class MovieAdapter(diffCallback: DiffUtil.ItemCallback<Movie>) :
    PagingDataAdapter<Movie, MovieAdapter.MovieViewHolder>(diffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieViewHolder {
        val listItemLayoutBinding = ListItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(listItemLayoutBinding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holder.bind(item)
        }
    }

    // comparator object
    object MovieComparator : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }

    /**
     * Movie view holder
     */
    class MovieViewHolder(private val binding: ListItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            // Movie data to ui components
            binding.movieTitle.text = movie.title
            binding.movieDescription.text = movie.overview
            binding.ratingBar.rating = movie.voteAverage!! / 2
            binding.movieRating.text = movie.voteAverage.toString()

            // Glide the movie poster onto view
            movie.posterPath?.run {
                Glide
                    .with(binding.root)
                    .load(Utilities.getImageUrl(this))
                    .into(binding.movieImage)
            }

            // On root click, go to the MovieDetailsFragment with the movie clicked as passed data
            binding.root.setOnClickListener {
                Navigation.findNavController(binding.root)
                    .navigate(
                        LatestMoviesFragmentDirections.actionLatestMoviesFragmentToMovieDetailsFragment(
                            movie
                        )
                    )
            }

        }
    }

}