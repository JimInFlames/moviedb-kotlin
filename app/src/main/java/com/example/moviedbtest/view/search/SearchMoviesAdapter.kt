package com.example.moviedbtest.view.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.databinding.SearchItemLayoutBinding

class SearchMoviesAdapter(var movies: List<Movie>) :
    RecyclerView.Adapter<SearchMoviesAdapter.SearchViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchViewHolder {
        val searchItemLayoutBinding =
            SearchItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(searchItemLayoutBinding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val item = movies[position]

        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class SearchViewHolder(private val binding: SearchItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            binding.movieTitle.text = movie.title

            /**
             * On search item click, move from search fragment to movie details fragment, passing the movie clicked
             */
            binding.root.setOnClickListener {
                Navigation.findNavController(binding.root)
                    .navigate(
                        SearchMoviesFragmentDirections.actionSearchFragmentToMovieDetailsFragment(
                            movie
                        )
                    )
            }
        }
    }

}