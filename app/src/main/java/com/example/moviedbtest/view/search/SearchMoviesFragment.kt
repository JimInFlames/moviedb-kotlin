package com.example.moviedbtest.view.search

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedbtest.databinding.FragmentSearchMoviesBinding
import org.koin.android.ext.android.inject


class SearchMoviesFragment : Fragment() {

    private val viewModel: SearchMoviesViewModel by inject()
    private lateinit var adapter: SearchMoviesAdapter
    private lateinit var binding: FragmentSearchMoviesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * Notify data set whenever this list changes
         */
        viewModel.movies.observe(this, {
            adapter.movies = it
            adapter.notifyDataSetChanged()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchMoviesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerViewSearchResult.layoutManager = LinearLayoutManager(context)
        adapter = SearchMoviesAdapter(emptyList())
        binding.recyclerViewSearchResult.adapter = adapter

        autoOpenKeyboard()

        binding.toolbarSearch.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty())
                    viewModel.searchBetweenMovies(s.toString())
                if (s != null) {
                    if (s.isEmpty())
                        adapter.movies = emptyList()
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

    }

    /**
     * When fragment view initializes, auto open keyboard
     */
    private fun autoOpenKeyboard() {
        binding.toolbarSearch.editTextSearch.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(binding.toolbarSearch.editTextSearch, InputMethodManager.SHOW_IMPLICIT)
    }

}