package com.example.moviedbtest.view.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.data.repository.MovieRepository
import kotlinx.coroutines.launch

class SearchMoviesViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val _movies: MutableLiveData<List<Movie>> = MutableLiveData()

    var movies: LiveData<List<Movie>>
        get() {
            return _movies
        }
        set(value) {}

    fun searchBetweenMovies(queryString: String) {
        viewModelScope.launch {
            _movies.value = movieRepository.searchMovies(queryString).results
        }
    }
}