package com.example.moviedbtest.datasource

import com.example.moviedbtest.data.model.Movie
import com.example.moviedbtest.network.response.MovieListResponse

fun fakeMovieListResponse(): MovieListResponse {
    val response = MovieListResponse()
    val movie1 = Movie(id = 1, title = "The Lion")
    val movie2 = Movie(id = 2, title = "Tenacious D")
    val movie3 = Movie(id = 3, title = "Jani dushman")
    val movie4 = Movie(id = 4, title = "Rain in the Garden")
    val movie5 = Movie(id = 5, title = "Dil garden")
    val movie6 = Movie(id = 6, title = "Dil garden")

    response.results = arrayListOf(movie1, movie2, movie3, movie4, movie5, movie6)

    return response
}
